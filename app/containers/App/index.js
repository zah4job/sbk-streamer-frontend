import React from 'react';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';

const host = '/stream'


export default class App extends React.Component {
  constructor(props) {
    super(props);
    const queryParameters = new URLSearchParams(window.location.search)
    this.state = {
      selectedSource: 'cam1',
      videoUrl: `${host}/hls/cam1.m3u8?${queryParameters}`,
    };
    this.changeSource = this.changeSource.bind(this);
  }

  // Instantiate a Video.js player when the component mounts
  componentDidMount() {
    this.player = videojs(this.videoNode, this.props, () => {
      videojs.log('onPlayerReady', this);
    });
  }

  // Dispose the player when the component will unmount
  componentWillUnmount() {
    if (this.player) {
      this.player.dispose();
    }
  }

  changeSource(e) {
    console.log(e.target.value);
    const camName = e.target.value;
    const queryParameters = new URLSearchParams(window.location.search)
    const newUrl = `${host}/hls/${camName}.m3u8?${queryParameters}`;
    console.log(`newUrl: ${newUrl}`);
    this.setState({
      selectedSource: e.target.value,
      videoUrl: newUrl,
    });
    console.log(this.player);
    this.player.src(newUrl);
    this.player.load();
    this.player.play();
  }

  // Wrap the player in a `div` with a `data-vjs-player` attribute, so Video.js
  // won't create additional wrapper in the DOM.
  //
  // See: https://github.com/videojs/video.js/pull/3856
  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <form style={{ display: 'inline-block', margin: '20px' }}>
          <div style={{ display: 'flex' }}>
            <div className="radio" style={{ marginRight: '100px' }}>
              <label>
                <input
                  type="radio"
                  value="cam1"
                  checked={this.state.selectedSource === 'cam1'}
                  onChange={this.changeSource}
                />
                Игровая
              </label>
            </div>
            <div className="radio">
              <label>
                <input
                  type="radio"
                  value="cam2"
                  checked={this.state.selectedSource === 'cam2'}
                  onChange={this.changeSource}
                />
                Класс
              </label>
            </div>
          </div>
        </form>
        <div>---</div>
        <div data-vjs-player style={{width: "100vw"}}>
          <video
            ref={node => (this.videoNode = node)}
            className="video-js"
            controls="controls"
          >
            <source src={this.state.videoUrl} type="application/x-mpegURL" />
          </video>
        </div>
      </div>
    );
  }
}
